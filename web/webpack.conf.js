var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: [
        './main/js/app/App.jsx'
    ],
    output: {
        path: __dirname + '/main/dist/js/',
        filename: 'App.build.js',
        publicPath: '/dist/js/'
    },
    resolve: {
        root: __dirname + '/main/js',
        modulesDirectories: ['node_modules'],
        unsafeCache: /node_modules/
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                include: [
                    path.resolve(__dirname, './main/js/app')
                ],
                exclude: /node_modules/, // to exclude the /main/dist directory
                query: {
                    presets: [
                        'react', 'es2015'
                    ]
                }
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            React: 'react',
            ReactBootstrap: 'react-bootstrap',
            Flux: 'flux',
            Events: 'events'
        })
    ]
};
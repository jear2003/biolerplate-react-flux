import ToDoList from '../../components/to-do-list/to-do-list.jsx';

// var Grid = ReactBootstrap.Grid; // this is soo heavy let's find a way to optimize it
let {
		Grid,
		Row,
		Col,
		ListGroup,
		ListGroupItem
	} = ReactBootstrap;

class Scaffold extends React.Component {
	render () {
		return  <Grid>
					<Row>
						<Col>
							<ToDoList/>
						</Col>
					</Row>
				</Grid>
	}
}

export default Scaffold;
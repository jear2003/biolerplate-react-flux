// var Grid = ReactBootstrap.Grid; // this is soo heavy let's find a way to optimize it

let {
		Grid,
		Row,
		Col,
		ListGroup,
		ListGroupItem
	} = ReactBootstrap;

class ToDoListItems extends React.Component {
	render () {

		let tasks = this.props.tasks.map(function (task, index) {
			return (<ListGroupItem key={index}>{task}</ListGroupItem>);
		});

		return  <Grid>
					<Row>
						<Col>
							<ListGroup>
								{tasks}
							</ListGroup>
						</Col>
					</Row>
				</Grid>
	}
}

export default ToDoListItems;
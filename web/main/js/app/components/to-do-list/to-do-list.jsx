import ToDoListItems from './to-do-list-items.jsx';
import ToDoListForm from './to-do-list-form.jsx';
import ToDoListStore from './to-do-list-store';

class ToDoList extends React.Component{

	constructor () {
		super();
		this.state = {
			tasks: this._getTasks()
		}
	}

	render () {
		return  <section>
					<ToDoListItems tasks={this.state.tasks}/>
					<ToDoListForm/>
				</section>
	}

	componentDidMount () {
		ToDoListStore.on('CHANGE', this._onChange.bind(this));
	}

	_onChange () {
		var self = this;
		// debugger;
		this.setState(this._getTasks());
		console.log('On Change private');
	}

	_getTasks () {
		let tasks = ToDoListStore.getAll();
		return tasks;
	}
}

export default ToDoList;
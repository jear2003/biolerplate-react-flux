import Dispatcher from '../../dispatcher/dispatcher';

var EventEmitter = Events.EventEmitter;

let ToDoListStore = Object.assign({}, EventEmitter.prototype, {
    tasks: [],
    getAll: function () {
        return this.tasks;
    },
    addTask: function (task) {
        this.tasks.push(task);
        this.emit('CHANGE');
    }
});

Dispatcher.register(function (payload) {
	switch (payload.eventName) {
		case 'add-tasks':
			ToDoListStore.addTask(payload.data.taskName);
		break;
	}
});

export default ToDoListStore;
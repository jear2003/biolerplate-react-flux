import Dispatcher from '../../dispatcher/dispatcher';
import ToDoListStore from './to-do-list-store';

let {
		Grid,
		Row,
		Col,
		ListGroup,
		ListGroupItem,
		Input,
		ButtonInput
	} = ReactBootstrap;

class ToDoListForm extends React.Component {
	render () {
		return  <form onSubmit={this._submit.bind(this)}>
					<Input ref="inputTask" type="text" label="Item" placeholder="Enter a new Task" />
					<ButtonInput type="submit" value="Submit Button" />
				</form>
	}

	_submit (event) {
		event.preventDefault();

		let task = ReactDOM.findDOMNode(this.refs.inputTask);
		task = task.getElementsByTagName('input')[0].value;

		Dispatcher.dispatch({
			eventName: 'add-tasks',
			data: {
				taskName: task
			}
		});
	}
}

export default ToDoListForm;